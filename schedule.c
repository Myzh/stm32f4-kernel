#include "schedule.h"

#include <stddef.h>


typedef struct {

  uint32_t data[2];
  
} stackFrame;


typedef union {

  struct emptyLogicalFrame {
    
    uint32_t padding; // 0x00
    void *lr;         // 0x04
  } logicalFrame;
  
  stackFrame frames[sizeof(struct emptyLogicalFrame) / sizeof(stackFrame)];
  
} emptyFrame;


typedef union {

  struct handlerLogicalFrame {
    
    uint32_t r0;   // 0x00
    uint32_t r1;   // 0x04
    uint32_t r2;   // 0x08
    uint32_t r3;   // 0x0c
    uint32_t r12;  // 0x10
    void *lr;      // 0x14
    void *pc; 		 // 0x18
    xPSR_Type psr; // 0x1c
  } logicalFrame;
  
  stackFrame frames[sizeof(struct handlerLogicalFrame) / sizeof(stackFrame)];
  
} handlerFrame;


// init for timer 7
static void initTimer();

// task entrypoint function
void runTask();

// ISR for timer 7
void TIM7_IRQHandler();

// ISR for reset
void Reset_Handler();


// global status vars
uint8_t taskOffset;
bool firstTask;


void scheduleInit() {
  
  taskOffset = 0;
  firstTask = true;
  
  for (uint8_t i = 0; i < NUM_TASKS; i++) {

    task *current = tasks + i;
    
    if (current->threadStack == NULL) {
    
      continue;
    }
    
    current->threadStack -= sizeof(emptyFrame);
    emptyFrame *tFrame = current->threadStack;
    
    current->threadStack -= sizeof(handlerFrame);
    handlerFrame *dFrame = current->threadStack;
    
    current->threadStack -= sizeof(handlerFrame);
    handlerFrame *sFrame = current->threadStack;
    
    tFrame->logicalFrame.lr = &Reset_Handler;
    
    dFrame->logicalFrame.lr = &runTask;
    dFrame->logicalFrame.pc = &runTask;
    dFrame->logicalFrame.psr.w = 0x21000000;
    
    sFrame->logicalFrame.lr = (void *)0xfffffff9;
    sFrame->logicalFrame.pc = &TIM7_IRQHandler + 0x0A;
    sFrame->logicalFrame.psr.w = 0x21000047;
  }
  
  NVIC_SetPriority(PendSV_IRQn, 10);
  NVIC_EnableIRQ(PendSV_IRQn);
  
  initTimer();
  
  __WFI();
}


static void initTimer() {
  
  // peripheral enable
  RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
  
  // enable the counter
  TIM7->CR1 |= (1 << 0);
  
  // set the prescaler and the auto reload register
  TIM7->PSC = 0xffff;
  TIM7->ARR = 0x00ff;
  
  // enable interrupts
  TIM7->DIER |= (1 << 0);
  
  // enable the interrupt in the NVIC and set priority
  NVIC_SetPriority(TIM7_IRQn, 5);
  NVIC_EnableIRQ(TIM7_IRQn);
  
  // call the scheduler instantly
  TIM7->SR |= (1 << 0);
}


void runTask() {
  
  // call the task function
  tasks[taskOffset / sizeof(task)].taskFunc();
}
