#include "main.h"

#include <stddef.h>

#include "schedule.h"


static void initLed() {

  // enable GPIO D ports
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
  
  // set the required pins as gerneral purpose output
  GPIOD->MODER &= ~((3 << (12 * 2)) | (3 << (13 * 2)) | (3 << (14 * 2)) | (3 << (15 * 2)));
  GPIOD->MODER |= (1 << (12 * 2)) | (1 << (13 * 2)) | (1 << (14 * 2)) | (1 << (15 * 2));

}


static void setLed(uint8_t led) {

  GPIOD->ODR &= ~((3 << 12) | (3 << 13) | (3 << 14) | (3 << 15));
  
  GPIOD->ODR |= (1 << led);
}


void taskA() {

  while (true) {
  
    setLed(12);
  }
  
}


void taskB() {

  while (true) {
  
    setLed(13);
  }
  
}


void taskC() {

  while (true) {
  
    setLed(14);
  }
  
}


void taskD() {

  while (true) {
  
    setLed(15);
    
    exitTask();
  }
  
}


task tasks[NUM_TASKS];


// main function
int32_t main() {
  
  tasks[0].threadStack = (void *)(0x2000A000 + 0x0000);
  tasks[0].taskFunc = &taskA;
  tasks[1].threadStack = NULL;
  tasks[1].taskFunc = &taskB;
  tasks[2].threadStack = (void *)(0x2000A000 + 0x4000);
  tasks[2].taskFunc = &taskC;
  tasks[3].threadStack = (void *)(0x2000A000 + 0x6000);
  tasks[3].taskFunc = &taskD;
  
  initLed();
  scheduleInit();
  
  while (true) {
  
  }
}
