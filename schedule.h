#ifndef SCHEDULE_H
#define SCHEDULE_H

// include general headers
#include <stdbool.h>
#include <inttypes.h>

// include local headers
#include "STM32F4xx.h"
#include "scheduleConfig.h"


typedef struct {

  void (*taskFunc)(); // 0x00
  void *threadStack;  // 0x04
  
} task;


extern task tasks[NUM_TASKS];


// init for the scheduler
void scheduleInit();

// exit current task
void exitTask();

#endif